const formulario = document.querySelector('#formulario');
let divMsgs = document.getElementById("msgs");
const url = 'https://treinamento-ajax-api.herokuapp.com/messages';


const transformDiv = (name, message, id) => {
    // criando div responsável pelo post
    let divMsg = document.createElement('div');
    divMsg.classList.add('msg');
    divMsg.id= `${id}`;
    
    //código HTML da div
    divMsg.innerHTML = `<div class="linha">
    <div class="buttons">
    <button type="button" name="delOne" onclick=deleteThisMessage(${id}) /></button>
    <button type="button" name="editOne" onclick=editThisMessage(${id}) /></button></div>
    <div class="nome" id="n${id}">${name}</div></div>
    <div class="conteudo" id="m${id}">${message}</div>`

    divMsgs.appendChild(divMsg);
}

//GET
fetch(url)
.then(resp => resp.json())
.then(messages => {
    messages.forEach(element => {
        transformDiv(element.name, element.message, element.id)
    });
})
.catch(error => console.log(error))

//POST
formulario.addEventListener('submit', (e) => {
    e.preventDefault();
    let name = document.forms["formulario"]["name"].value;
    let message = document.forms["formulario"]["message"].value;
    if (name != '' && message != '') {
        let fetchBody = {
            "message": {
                "name": name,
                "message": message
            }
        }
        let fetchConfig = {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(fetchBody)
        }
        fetch(url, fetchConfig)
        .then(resp => resp.json())
        .then(element => transformDiv(element.name, element.message, element.id))
        .catch(error => console.warn(error))
    }
});

// PUT/PATCH
const editThisMessage = (e) => {
    let divMsg = document.getElementById(e);

    // criar espaço para edição da mensagem
    let edicao = document.createElement('div');
    edicao.classList.add('edicao');
    edicao.innerHTML=`<label for="name" class="boxE">
    Editar nome: <input class="text-edit area" type="text" name="name" id="novoNome${e}"/></label>
    <label for="message" class="boxE">Editar Mensagem: <textarea class="text-edit" type="text" name="message" id="novaMsg${e}" rows="5"></textarea></label> 
    <input type="button" name="edit" value="Editar" onclick="sendEdit(${e})"/>`
    
    // adicionando no box de mensagem
    if (divMsg.lastElementChild.className != 'edicao') {
        divMsg.appendChild(edicao);
    } else {
        fechar(e);
    }
}

const sendEdit = (id) => {
    name_edit = document.getElementById(`novoNome${id}`).value;
    message_edit = document.getElementById(`novaMsg${id}`).value;

    if (name_edit != '' && message_edit != '') {
        let fetchBody = {
            "message": {
                "name": name_edit,
                "message": message_edit
            } 
        }
        let fetchConfig = {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(fetchBody)
        }
        fetch(url+`/${id}`, fetchConfig)
        .then(resp => resp.json())
        .then(element => {
            document.querySelector(`#n${id}`).innerHTML = element.name
            document.querySelector(`#m${id}`).innerHTML = element.message})
        .catch(error => console.warn(error))
        fechar(id);
    }
}

const fechar = (id) => {
    document.getElementById(id).lastElementChild.remove();
}

// DELETE
const deleteThisMessage = (id) => {
    let fetchConfig = {
        method: "DELETE"
    }
    fetch(url+`/${id}`, fetchConfig)
    .then(() => document.getElementById(id).remove())
    .catch(error => console.warn(error))
};